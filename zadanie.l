%{
#include <string.h>

int it,ik,riadok=1;
extern YYSTYPE yylval;
int yywrap(void);
struct { char *txt;int kod;} ks[]={
	"applet",APPLET,
	"farba",FARBA,
	"int",INT,
	"boolean",BOOLEAN,
	"void",VOID,	
	
	"init",INIT,
	"move",MOVE,
	"setcolor",SETCOLOR,
	"resize",RESIZE,
	"show",SHOW,
	"line",LINE,
	"from",FROM,
	"to",TO,
	"in",IN,
	"print",PRINT,
	"at", AT,
	"if",IF,
	"else",ELSE,
	"while",WHILE,
	"do",DO,
	"for",FOR,
	"with",WITH,
	"color", COLOR,
	"output", OUTPUT,
	"end", END,
	"begin", BEGINN,

	"inti", INTI,
	"rectangle", RECTANGLE,
	"endapplet", ENDAPPLET,
	"painttt", PAINTTT,

	NULL,0
	};
struct { char *txt;char *meno;} farba[]={
			"BLACK","black",
			"BLUE","blue",
			"GREEN","green",
			"CYAN","cyan",
			"RED","red",
			"MAGENTA","magenta",
			"LIGHTGRAY","lightGray",
			"DARKGRAY","darkGray",
			"GRAY","gray",
			"ORANGE","orange",
			"PINK","pink",
			"YELLOW","yellow",
			"WHITE","white",
			NULL,NULL
	};

%}

P		[a-zA-Z]
C		[0-9]
IDENT		{P}({P}|{C})*
CIS		{C}+
BIELY		[ \t]
KOMENTAR	 "//".*
%%
"++"		return(PLUSPLUS);
"--"		return(MINUSMINUS);
","		return(CIARKA);
";"		return(BCIARKA);
"("		return(LOZAT);
")"		return(POZAT);
"{"		return(LZZAT);
"}"		return(PZZAT);
"["		return(LHZAT);
"]"		return(PHZAT);
"."		return(BODKA);
"="		return(ROVNE);
"+"		return(PLUS);
"-"		return(MINUS);
"*"		return(KRAT);
"/"		return(DEL);
"<"		return(MENSI);
"<="		return(MENSIROVNY);
">"		return(VACSI);
">="		return(VACSIROVNY);
"=="		return(EQUIVAL);
"!="		return(NEEQ);
"||"		return(OR);
"&&"		return(AND);
"%"		return(ZVYS);
"!"		return(NOT);
"?"		return(OTAZ);
":"		return(DBOD);
\"[^"\n]*\"	{ yytext[yyleng-1]=0;
		  yylval.txt=strdup(&yytext[1]);
		  return(TEXT);
		}
{IDENT}		{ /* test na klucove slovo */
		  for(ik=0;ks[ik].txt!=NULL;ik++)
			if(strcmp(ks[ik].txt,yytext)==0)return ks[ik].kod;

		  /* test na druh farby */
		  for(ik=0;farba[ik].txt!=NULL;ik++)
			if(strcmp(farba[ik].txt,yytext)==0)
			{
			    yylval.meno=strdup(farba[ik].meno);
			    return (FARBA);
			}

		  /* ked nic ine tak to bude identifikator*/
		  yylval.meno=strdup(yytext);
		  return(ID);
		}
{CIS}		{ sscanf(yytext,"%d",&yylval.ihod);return(INT_LITERAL);}
{BIELY}		;
\n		{ riadok++;}
{KOMENTAR}	;
.		{ xxerror("Nedovoleny znak: ",yytext);}
%%
int yywrap(void){return(1);}

