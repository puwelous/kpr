%{
	/* atributy vyrazu */
typedef struct {char *sig;char*kod;} ATRV;
%}

	/* definicia prenasanych hodnot */
%union  {
	char* meno;
	char* txt;
	char* sig;
	unsigned int mod;
	int ihod;
	double dhod;
	char* kod;
	ATRV vhod;
	}

%token		BOOLEAN  INT  VOID 
%token		RECTANGLE
%token	PAINTTT ENDAPPLET INTI 

%token		APPLET BEGINN END INIT MOVE SETCOLOR RESIZE SHOW LINE COLOR OUTPUT PRINT  
%token		DO IN AT TO FROM   
%token 	IF ELSE WHILE WITH FOR
%token <meno> 	ID FARBA
%token <txt>		TEXT
%token <ihod>   	INT_LITERAL
%token  	LZZAT  PZZAT  LOZAT  POZAT  LHZAT  PHZAT
%token  	CIARKA  BCIARKA  BODKA  ROVNE


%left	       OTAZ DBOD
%left		AND OR
%binary	MENSI MENSIROVNY VACSI VACSIROVNY EQUIVAL NEEQ ZVYS
%left		PLUS MINUS
%left		KRAT DEL
%left		PLUSPLUS MINUSMINUS
%left         UNARNY NOT

%type <sig>	TypDat TypVoid	
%type <vhod>	Primarny Vyraz VoidVyraz
%type <kod>   VyrazPr Prikaz Prikazy 


%{
typedef unsigned int uint;
typedef	struct { uint pristup;
		char *meno;
		char *signatura;
		} dclen;
typedef	struct { uint pristup;
		char *meno;
		char *signaturaP;char *signaturaR;
		int velkostLP;
		char *kod;
		} met;

struct {
	char *menoTr; char *javaTr;char *sigTr;
	char *menoNadTr; int ixntr;
	uint pristup;
	int pocDatClenov;
	dclen* DatCleny;
	int pocMetod;
	met* Metody;
} triedy[20];

int ix_tr=0;
int ix_m=0;

struct { char *meno; char *sig; int index;} LP[20];
int ixlp=0;
int szlp=0;

int navestie=1,pn1,pn2;


#define MOD_PUBLIC    0x0001
#define MOD_PRIVATE   0x0002
#define MOD_PROTECTED 0x0004
#define MOD_STATIC    0x0008
#define MOD_FINAL     0x0010
#define MOD_NATIVE    0x0100
#define MOD_ABSTRACT  0x0400


#include <alloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

FILE *vyst;
int ii,i1,i2,n1,n2;
char *sig;
char sigpom[80];
char vst_subor[20] = "zadanie";
static char src[30];
static char vstup[30];


/* prototypy */
int yyparse(void), yylex(void);

void ulozImp(char *,char *);
void ulozTr(char *,int,uint);
void ulozDC(char *,char *,uint);
void ulozMet(char *,char *,char *,uint);
int testEM(char *m,int ix);	
int testEDC(char *m,int ix);

void yyerror(char*),xxerror(char*,char*);
void vypisTriedy(int);

int najdiDC(char *m,int ix,int *ixx);
int najdiM(char *m,char *sg,int ix,int *ixx);
int najdiT(char *sg);
void ulozLP(char *meno,char *sig);
void initLP(int mod);
void ukoncLP(void);
int najdiLP(char *meno);

ATRV binAritVyraz(ATRV v1,ATRV v2,char* int_op,char* dbl_op);
ATRV binRelVyraz(ATRV v1,ATRV v2,char* int_op,char* dbl_op,char* obj_op);
ATRV binLogVyraz(ATRV v1,ATRV v2,char* logop);
ATRV unAritVyraz(ATRV v1,char* int_op,char* dbl_op);


#include "lexyy.cpp"
%}

%%
Subor		: Program
		;
		/* Program -> applet id paint Deklaracie  Prikazy endapplet */
		/* APPLET LOZAT ID POZAT Deklaracie BEGINN Prikazy END      */
Program	: APPLET ID PAINTTT Deklaracie Prikazy ENDAPPLET
		
			{
			ukoncLP();
			triedy[ix_tr-1].Metody[ix_m].kod=$5;
			strcat(triedy[ix_tr-1].Metody[ix_m].kod,"\treturn\n");
			}



		
		;
Deklaracie	: /* e */
		| TypDat ID BCIARKA
			{
			if(strcmp($1,"I")==0) ulozLP($2,$1);
			else ulozDC($2,$1,MOD_PUBLIC);
			}
		| Deklaracie TypDat ID BCIARKA
			{
			if(strcmp($2,"I")==0) ulozLP($3,$2);
			else ulozDC($3,$2,MOD_PUBLIC);
			}
		;
TypVoid	: VOID
			{$$="V";}
		;
TypDat		: INT
			{$$="I";}
		
		| RECTANGLE
			{$$="R";}
		;
Prikazy 	: /* e */ {$$=(char*)malloc(8000);$$[0]=0;}
		| Prikazy Prikaz
			{if($2!=NULL){$$=strcat($1,$2);free($2);}
			 else $$=$1;
			}
Prikaz	: VyrazPr BCIARKA
			{$$=$1;}
		| BCIARKA
			{$$=NULL;}
		| LZZAT Prikazy PZZAT
			{$$=$2;}

		
		| WHILE LOZAT Vyraz POZAT Prikaz
			{
				if($3.sig[0]!='Z')
					yyerror("Ocakava sa log.vyraz pre WHILE");
			 	$$=(char*)malloc(50+strlen($3.kod)+strlen($5));
				pn1=navestie++;
				pn2=navestie++;

				sprintf($$,		  ".%d\n",pn1);
				sprintf($$+strlen($$),"%s\tifeq\t.%d\n",$3.kod,pn2);
				sprintf($$+strlen($$),"%s\tgoto\t.%d\n",$5,pn1);
				sprintf($$+strlen($$),".%d\n",pn2);

				free($3.kod);
				free($5);
			}
		| DO Prikaz WHILE LOZAT Vyraz POZAT BCIARKA
			{
				if($5.sig[0]!='Z')
					yyerror("Ocakava sa log.vyraz pre WHILE");
				$$=(char*)malloc(50+strlen($5.kod)+strlen($2));
				pn1=navestie++;

				sprintf($$,		 ".%d\n",pn1);
				sprintf($$+strlen($$),"%s\n",$2);
				sprintf($$+strlen($$),"%s\tifne\t.%d\n",$5.kod,pn1);
				
				free($5.kod);
				free($2);
			}


		| FOR LOZAT Vyraz BCIARKA Vyraz BCIARKA VoidVyraz POZAT Prikaz
			{
				if($5.sig[0]!='Z')
					yyerror("Ocakava sa log.vyraz pre FOR");
				$$=(char*)malloc(200+
32+
strlen($5.kod)+
strlen($7.kod)+
strlen($9));
       	      		pn1=navestie++; 
				pn2=navestie++; 
				
				sprintf($$,		 "\t%s",$3); 				/*inicializacia*/
				sprintf($$+strlen($$),"\t.%d\n",pn1);   			/*Navestie 1*/
				sprintf($$+strlen($$),"%s",$5.kod);			/*Vykonaj podmienku*/
				sprintf($$+strlen($$),"\tifeq\t.%d\n",pn2);		/*testuje ci je vysledok podmienky 0. ak hej, skace na koniec*/
				sprintf($$+strlen($$),"%s",$9);				/*Vykona obsah*/
				sprintf($$+strlen($$),"%s",$7.kod);			/*Vykona zvycajne "inkrementaciu"*/
				sprintf($$+strlen($$),"\tgoto\t.%d\n",pn1);		/*Navrat na zaciatok cyklu*/
				sprintf($$+strlen($$),"\t.%d\n",pn2);   			/*Navestie 2*/				

				/*free($3.kod); //???*/
				free($5.kod);
				free($7.kod);
				free($9);
			}
		
		


		| IF LOZAT Vyraz POZAT Prikaz ELSE Prikaz
			{
				if($3.sig[0]!='Z')
					yyerror("Ocakava sa log.vyraz pre IF");
			 	$$=(char*)malloc(50+strlen($3.kod)+strlen($5)+strlen($7));
				pn1=navestie++;
				pn2=navestie++;

			 	sprintf($$,		 "%s\tifeq\t.%d\n",$3.kod,pn1);
			  	sprintf($$+strlen($$),"%s\tgoto\t.%d\n",$5,pn2);
			  	sprintf($$+strlen($$),".%d\n",pn1);
			  	sprintf($$+strlen($$),"%s.%d\n",$7,pn2);

				free($3.kod);
				free($5);
				free($7);
			}


		| IF LOZAT Vyraz POZAT Prikaz
			{	
				if($3.sig[0]!='Z')
					yyerror("Ocakava sa log.vyraz pre IF");
				$$=(char*)malloc(50+strlen($3.kod)+strlen($5));
				pn1=navestie++;

			  	sprintf($$,
					
					"%s\tifeq\t.%d\n"
			  		"%s.%d\n"
					,$3.kod,pn1
					,$5,pn1);

				free($3.kod);
				free($5);
			} 

		/*Inicializacia -> init(id,x,y,a,farba)*/
/*		| INIT LOZAT ID CIARKA Vyraz CIARKA Vyraz CIARKA Vyraz CIARKA FARBA POZAT BCIARKA */

		/* obd.inti(300, 300, 50, 50, RED); */
		| ID BODKA INTI LOZAT Vyraz CIARKA  Vyraz CIARKA Vyraz CIARKA Vyraz CIARKA FARBA POZAT BCIARKA

			{	
				{
				$$ = (char*)malloc(496);
				sprintf($$,  
						"\taload_0\n%s\tputfield %s %s_Lx I\n"
						"\taload_0\n%s\tputfield %s %s_Ly I\n"
						"\taload_0\n%s\tputfield %s %s_a I\n"
						"\taload_0\n%s\tputfield %s %s_b I\n"
						"\taload_0\n\tgetstatic java/awt/Color %s Ljava/awt/Color;\n\tputfield %s %s_Col Ljava/awt/Color;\n",
						$5.kod, vst_subor, $1, 
						$7.kod, vst_subor, $1,
						$9.kod, vst_subor, $1,
						$11.kod, vst_subor, $1,
						$13, vst_subor, $1);
				}	
			}			

		/* Presun -> move(id,x,y), presun (na relativnu poziciu)*/
		/*          move id to (x,y)                            */
		/* MOVE LOZAT ID CIARKA Vyraz CIARKA Vyraz POZAT BCIARKA */
		| MOVE ID TO LOZAT Vyraz CIARKA Vyraz POZAT BCIARKA
			{
			  $$=(char*)malloc(696);
			  sprintf($$,"\taload_0\n\taload_0\n\tgetfield %s %s_Lx I\n%s\tiadd\n\tputfield %s %s_Lx I\n"
					  "\taload_0\n\taload_0\n\tgetfield %s %s_Ly I\n%s\tiadd\n\tputfield %s %s_Ly I\n"
					 ,vst_subor,$2,$5.kod,vst_subor,$2,vst_subor,$2,$7.kod,vst_subor,$2);  
			}

		/* ZmenaFarby -> setcolor id to farba  rovnake uplne*/
		/* setcolor id to farba */
		| SETCOLOR ID TO FARBA BCIARKA
			{
			  	$$=(char*)malloc(800);

				sprintf($$,		 "\taload_0\n");
			  	sprintf($$+strlen($$),"\tgetstatic java/awt/Color %s Ljava/awt/Color;\n",$4);
			  	sprintf($$+strlen($$),"\tputfield %s %s_Col Ljava/awt/Color;\n",vst_subor,$2);  
			}

		/* ZmenaVelkosti -> id.resize(a), zmena velkosti (relativne) */
		/* resize(id,x,y) */
		/* ID BODKA RESIZE LOZAT Vyraz POZAT BCIARKA	*/
		| RESIZE LOZAT ID CIARKA Vyraz CIARKA Vyraz POZAT BCIARKA
			{
			  $$=(char*)malloc(696);
			  sprintf($$,"\taload_0\n\taload_0\n\tgetfield %s %s_a I\n%s\tiadd\n\tputfield %s %s_a I\n"
					  "\taload_0\n\taload_0\n\tgetfield %s %s_b I\n %s\tiadd\n\tputfield %s %s_b I\n"
					 ,vst_subor, $3, $5.kod, vst_subor, $3, vst_subor, $3, $7.kod, vst_subor, $3); 
			
			}

		/* Vykreslenie -> id.show() */
		/*  show(id)  */
		/* ID BODKA SHOW LOZAT POZAT BCIARKA */
		| SHOW LOZAT ID POZAT BCIARKA
		
			{	
				$$ = (char*)malloc(800);
				sprintf($$,
						"\taload\t1\n"
						"\taload_0\n\tgetfield %s %s_Col Ljava/awt/Color;\n"
						"\tinvokevirtual  java/awt/Graphics setColor (Ljava/awt/Color;)V\n"
						"\taload\t1\n"
						"\taload_0\n\tgetfield %s %s_Lx I\n"
						"\taload_0\n\tgetfield %s %s_Ly I\n"
						"\taload_0\n\tgetfield %s %s_a I\n"	
						"\taload_0\n\tgetfield %s %s_b I\n"
						"\tinvokevirtual  java/awt/Graphics fillRect (IIII)V\n",
						vst_subor, $3,
						vst_subor, $3,
						vst_subor, $3,
						vst_subor, $3,
						vst_subor, $3,
						vst_subor, $3);

			}	



		/* ZobrazCiaru -> line(x,y,x,y,farba) uplne rovnake
		   1     2     3     4      5     6      7     8      9     10     11    12    13  */
         	| LINE LOZAT Vyraz CIARKA Vyraz CIARKA Vyraz CIARKA Vyraz CIARKA FARBA POZAT BCIARKA
			{
				$$ = (char*)malloc(800);
				sprintf($$,
						"\taload\t1\n\tgetstatic java/awt/Color %s Ljava/awt/Color;\n"
						"\tinvokevirtual  java/awt/Graphics setColor (Ljava/awt/Color;)V\n"
						"\taload\t1\n"
						"%s"
						"%s"
						"%s"
						"%s"
						"\tinvokevirtual  java/awt/Graphics drawLine (IIII)V\n",
						$11, 
						$3.kod, 
						$5.kod, 
						$7.kod, 
						$9.kod); 
		}

		/* ZobrazText -> output(x,y) TEXT color farba vs. print (x,y,farba) TEXT
		     1     2     3      4     5     6    7     8     9     10  */
		/*  OUTPUT LOZAT Vyraz CIARKA Vyraz POZAT TEXT COLOR FARBA BCIARKA */
		| PRINT LOZAT Vyraz CIARKA Vyraz CIARKA FARBA POZAT TEXT BCIARKA
			{	$$ = (char*)malloc(5800);
				sprintf($$,
						
						"\taload\t1\n"
						"\tnew\tjava/awt/Font\n"
						"\tdup\n"
						"\tldc\t\"Helvetica\"\n"
						"\tgetstatic\tjava/awt/Font\tBOLD I\n"
						"\tsipush\t20\n"
						"\tinvokespecial\tjava/awt/Font <init>\t(Ljava/lang/String;II)V\n"
						"\tinvokevirtual\tjava/awt/Graphics\tsetFont\t(Ljava/awt/Font;)V\n"
						"\taload\t1\n\tgetstatic java/awt/Color %s Ljava/awt/Color;\n"
						"\tinvokevirtual  java/awt/Graphics setColor (Ljava/awt/Color;)V\n"
						"\taload\t1\n"
						"\tldc\t\"%s\"\n"
						"%s%s"
						"\tinvokevirtual java/awt/Graphics drawString (Ljava/lang/String;II)V\n",
   						$7, 
						$9, 
						$3.kod, 
						$5.kod);	
			}
		;



VoidVyraz	: Vyraz
			{$$.sig="V";
			 $$.kod=(char*)malloc(12+strlen($1.kod));
			 if($1.sig[0]=='V')sprintf($$.kod,"%s",$1.kod);
			 else if($1.sig[0]=='D'||$1.sig[0]=='J')sprintf($$.kod,"%s\tpop2\n",$1.kod);
			 else sprintf($$.kod,"%s\tpop\n",$1.kod);
			 free($1.kod);
			}

Vyraz		: Primarny		{$$=$1;}
		| Vyraz PLUS Vyraz
			{$$=binAritVyraz($1,$3,"iadd","dadd");}
		| Vyraz MINUS Vyraz 
			{$$=binAritVyraz($1,$3,"isub","dsub");}
		| Vyraz KRAT Vyraz
			{$$=binAritVyraz($1,$3,"imul","dmul");}
		| Vyraz DEL Vyraz 
			{$$=binAritVyraz($1,$3,"idiv","ddiv");}
		| Vyraz MENSI Vyraz
			{$$=binRelVyraz($1,$3,"if_icmplt","dcmpl\n\tiflt",NULL);}
		| Vyraz MENSIROVNY Vyraz
			{$$=binRelVyraz($1,$3,"if_icmple","dcmpl\n\tifle",NULL);}
		| Vyraz VACSI Vyraz
			{$$=binRelVyraz($1,$3,"if_icmpgt","dcmpl\n\tifgt",NULL);}
		| Vyraz VACSIROVNY Vyraz
			{$$=binRelVyraz($1,$3,"if_icmpge","dcmpl\n\tifge",NULL);}
		| Vyraz EQUIVAL Vyraz
			{$$=binRelVyraz($1,$3,"if_icmpeq","dcmpl\n\tifeq","if_acmpeq");}
		| Vyraz NEEQ Vyraz
			{$$=binRelVyraz($1,$3,"if_icmpne","dcmpl\n\tifne","if_acmpne");}
		| Vyraz OR Vyraz
			{$$=binLogVyraz($1,$3,"ior");}
		| Vyraz AND Vyraz
			{ $$=binLogVyraz($1,$3,"iand");}
		| PLUS Vyraz %prec UNARNY
			{$$=unAritVyraz($2,NULL,NULL);}
		| MINUS Vyraz %prec UNARNY
			{$$=unAritVyraz($2,"ineg","dneg");}
		| Vyraz ZVYS Vyraz 
			{$$=binAritVyraz($1,$3,"irem","drem");}
				| ID PLUSPLUS
			{
				ii=najdiLP($1);
				$$.sig=LP[ii].sig;
				$$.kod=(char*)malloc(20);
			
				if(LP[ii].sig[0]=='L')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, 1);
				if(LP[ii].sig[0]=='I')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, 1);
				if(LP[ii].sig[0]=='Z')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, 1);
				if(LP[ii].sig[0]=='D')sprintf($$.kod,"\tdload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, 1.0);
			}
		| PLUSPLUS ID
			{
				ii=najdiLP($2);
				$$.sig=LP[ii].sig;
				$$.kod=(char*)malloc(20);

				if(LP[ii].sig[0]=='L')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, 1,LP[ii].index);
				if(LP[ii].sig[0]=='I')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, 1,LP[ii].index);
				if(LP[ii].sig[0]=='Z')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, 1 ,LP[ii].index);
				if(LP[ii].sig[0]=='D')sprintf($$.kod,"\tiinc\t%d\t%d\n\tdload\t%d\n",LP[ii].index, 1.0,LP[ii].index);
			}
		| ID MINUSMINUS
			{
				ii=najdiLP($1);
				$$.sig=LP[ii].sig;
				$$.kod=(char*)malloc(20);

				if(LP[ii].sig[0]=='L')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, -1);
				if(LP[ii].sig[0]=='I')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, -1);
				if(LP[ii].sig[0]=='Z')sprintf($$.kod,"\tiload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, -1);
				if(LP[ii].sig[0]=='D')sprintf($$.kod,"\tdload\t%d\n\tiinc\t%d\t%d\n",LP[ii].index,LP[ii].index, -1.0);

			}
		| MINUSMINUS ID
			{
				ii=najdiLP($2);
				$$.sig=LP[ii].sig;
				$$.kod=(char*)malloc(20);

				if(LP[ii].sig[0]=='L')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, -1,LP[ii].index);
				if(LP[ii].sig[0]=='I')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, -1,LP[ii].index);
				if(LP[ii].sig[0]=='Z')sprintf($$.kod,"\tiinc\t%d\t%d\n\tiload\t%d\n",LP[ii].index, -1 ,LP[ii].index);
				if(LP[ii].sig[0]=='D')sprintf($$.kod,"\tiinc\t%d\t%d\n\tdload\t%d\n",LP[ii].index, -1.0,LP[ii].index);
			}
		| NOT Vyraz BCIARKA
			{	
				if($2.sig[0]=='Z')
				{
					$$.sig=$2.sig;
					$$.kod=(char*)malloc(35+strlen($2.kod));
					sprintf($$.kod,"%s\ticonst_1\n\tixor\n",$2.kod);

					free($2.kod);
				}	
				else
					xxerror("Negacia vyrazu",$2.kod);
			}
		| Vyraz OTAZ Vyraz DBOD Vyraz
			{
				if($1.sig[0]!='Z')
				{
					putchar($1.sig[0]);
					xxerror("Nezadany logicky vyraz",$1.kod);
				}

				if($3.sig[0]!=$5.sig[0])
					yyerror("Nespravne typy vyrazov");

				$$.sig[0]=$3.sig[0];
				
				pn1=navestie++;
				pn2=navestie++;

				$$.kod=(char*)malloc(100+strlen($1.kod)+strlen($3.kod)+strlen($5.kod));

				sprintf($$.kod,		  "%s\tifeq\t.%d\n",$1.kod,pn1);
				sprintf($$.kod+strlen($$.kod),"%s\tgoto\t.%d\n",$3.kod,pn2);
				sprintf($$.kod+strlen($$.kod),".%d\n%s.%d\n",pn1,$5.kod,pn2);

				free($1.kod);
				free($3.kod);
				free($5.kod);
			}
		;
VyrazPr 	:  ID ROVNE Vyraz
			{ if((ii=najdiLP($1))>=0){
				$$=(char*)malloc(20+strlen($3.kod));
				if(strcmp(LP[ii].sig,$3.sig)!=0)yyerror("Nekompatibilne priradenie");
			  	if(LP[ii].sig[0]=='L')sprintf($$,"%s\tastore\t%d\n",$3.kod,LP[ii].index);
				else if(LP[ii].sig[0]=='I')sprintf($$,"%s\tistore\t%d\n",$3.kod,LP[ii].index);
				else if(LP[ii].sig[0]=='D')sprintf($$,"%s\tdstore\t%d\n",$3.kod,LP[ii].index);
			  	else if(LP[ii].sig[0]=='Z')sprintf($$,"%s\tistore\t%d\n",$3.kod,LP[ii].index);
				}
			  else {$$=NULL;xxerror("Neexistujuca lok.premenna",$1);}
			  free($3.kod);
			}
		;
Primarny	: ID
			{ if((ii=najdiLP($1))>=0){
				$$.sig=LP[ii].sig;$$.kod=(char*)malloc(20);
				if($$.sig[0]=='L')     sprintf($$.kod,"\taload\t%d\n",LP[ii].index);
				else if($$.sig[0]=='I')sprintf($$.kod,"\tiload\t%d\n",LP[ii].index);
				else if($$.sig[0]=='Z')sprintf($$.kod,"\tiload\t%d\n",LP[ii].index);
				else if($$.sig[0]=='D')sprintf($$.kod,"\tdload\t%d\n",LP[ii].index);
				}
			  else {$$.sig="I";$$.kod=strdup("\terror\n");xxerror("Neexistujuca lok.premenna",$1);}
			}
		| INT_LITERAL
			{ $$.sig="I";$$.kod=(char*)malloc(20);
			  sprintf($$.kod,"\tsipush\t%d\n",$1);
			}
		| LOZAT  Vyraz  POZAT
			{ $$=$2;}
		;		
%%

static int pocetChyb=0;

void yyerror(char *s)
{
	fprintf(stdout,"CHYBA: %s [%s:%d]\n",s,yytext,riadok);
	
	pocetChyb++;
}

void xxerror(char *s1,char *s2)
{
	fprintf(stdout,"CHYBA: %s %s [%d]\n",s1,s2,riadok);
	
	pocetChyb++;
}

int main(int ac,char *av[])
{
	extern FILE *yyin;
	int i;
	
	if(ac>1)
	{
		yyin=fopen(av[1],"r");strcpy(src,av[1]);
		for(i=0;(i<strlen(src))&&(src[i]!='.');i++)
			vstup[i]=src[i];
		vstup[i]=0;
		strcpy(vstup,"zadanie");

		if(yyin==NULL){yyin=stdin;strcpy(src,"stdin");}
	}

	ulozImp("Object","java/lang/Object");
	ulozImp("String","java/lang/String");
	ulozImp("System","java/lang/System");
	ulozImp("Graphics","java/awt/Graphics");
	ulozImp("Component","java/awt/Component");
	ulozImp("Container","java/awt/Container");
	ulozImp("Panel","java/awt/Panel");
	ulozImp("Applet","java/applet/Applet");
	ulozImp("Font","java/awt/Font");
	ulozImp("Window","java/awt/Window");
	ulozImp("Frame","java/awt/Frame");

	for(i=0;i<ix_tr;i++)
		if(strcmp(triedy[i].menoTr,"Applet")==0)
			ulozTr(vstup,i,MOD_PUBLIC);
	
	ulozMet("<init>","()","V",MOD_PUBLIC);
	ukoncLP();
	triedy[ix_tr-1].Metody[ix_m].kod=(char*)malloc(100);
	sprintf(triedy[ix_tr-1].Metody[ix_m].kod,
		"\taload_0\n\tinvokespecial\tjava/applet/Applet <init> ()V\n\treturn\n");
	
	initLP(MOD_PUBLIC & MOD_STATIC);
	for(i=0;i<ix_tr;i++)
		if(strcmp(triedy[i].menoTr,"Graphics")==0)
			ulozLP("g",triedy[i].sigTr);

	ulozMet("paint","(Ljava/awt/Graphics;)","V",MOD_PUBLIC);

	(void)yyparse();
	if (pocetChyb != 0){
	fprintf(stderr,"POCET CHYB: %d\n",pocetChyb);}
	else fprintf(stderr,"OK");
	for(i=0;i<ix_tr;i++)
		vypisTriedy(i);
	
	getchar();
	
	return(0);
}

char* ulozStr(char *s){
return strdup(s);
}

void ulozImp(char *meno,char *jmeno){
FILE *fi;char t[80],t1[80],t2[80],dt[10];int i,pdc,pm;uint pr;
fi=fopen("java.api","r");
if(fi==NULL){xxerror("Nemozno najst archiv","java.api");return;}
fscanf(fi,"%s",t);
while(strcmp(t,".end")!=0){
	if(strcmp(t,".class")!=0 && strcmp(t,".interface")!=0){fscanf(fi,"%s",t);continue;}
	fscanf(fi,"%x %s",&pr,t);if(strcmp(t,jmeno)!=0){fscanf(fi,"%s",t);continue;}
	triedy[ix_tr].pristup=pr|MOD_NATIVE;
	triedy[ix_tr].menoTr=meno;triedy[ix_tr].javaTr=jmeno;
	triedy[ix_tr].sigTr=(char*)malloc(strlen(jmeno)+3);
	sprintf(triedy[ix_tr].sigTr,"L%s;",jmeno);
	triedy[ix_tr].menoNadTr=NULL;triedy[ix_tr].ixntr= -1;
	fscanf(fi,"%s",t);
	if(strcmp(t,".super")==0){ fscanf(fi,"%s",t);
		for(i=0;i<ix_tr;i++)
			if(strcmp(triedy[i].menoTr,t)==0||strcmp(triedy[i].javaTr,t)==0){
				triedy[ix_tr].menoNadTr=triedy[i].menoTr;triedy[ix_tr].ixntr=i;
				}
		if(triedy[ix_tr].ixntr<0){xxerror("Nemozno najst nadtriedu:",t);}
		}
	fscanf(fi,"%d",&pdc);triedy[ix_tr].pocDatClenov=pdc;
	if(pdc>0)triedy[ix_tr].DatCleny=(dclen*)calloc(pdc,sizeof(dclen));
	for(i=0;i<pdc;i++){ fscanf(fi,"%s %x %s %s\n",dt,&pr,t1,t2);
			triedy[ix_tr].DatCleny[i].meno=ulozStr(t1);
			triedy[ix_tr].DatCleny[i].signatura=ulozStr(t2);
			triedy[ix_tr].DatCleny[i].pristup=pr;
			}
	fscanf(fi,"%d",&pm);triedy[ix_tr].pocMetod=pm;
	if(pm>0)triedy[ix_tr].Metody=(met*)calloc(pm,sizeof(met));
	for(i=0;i<pm;i++){ fscanf(fi,"%s %x %s %s %s\n",dt,&pr,t,t1,t2);
			triedy[ix_tr].Metody[i].pristup=pr;
			triedy[ix_tr].Metody[i].meno=ulozStr(t);
			triedy[ix_tr].Metody[i].signaturaP=ulozStr(t1);
			triedy[ix_tr].Metody[i].signaturaR=ulozStr(t2);
			}
	ix_tr++;
	fclose(fi);return;
	}
fclose(fi);
xxerror("Nemozno najst JAVA triedu:",jmeno);
}

void ulozTr(char *meno,int ixNT,uint mod)
{
	int i,j,k;
	triedy[ix_tr].menoTr=meno;triedy[ix_tr].javaTr=meno;
	triedy[ix_tr].sigTr=(char*)malloc(strlen(meno)+3);
	sprintf(triedy[ix_tr].sigTr,"L%s;",meno);
	triedy[ix_tr].ixntr= ixNT;

	if(triedy[ixNT].pristup&MOD_FINAL)
		xxerror("Nemozno dedit z FINAL triedy",triedy[ixNT].menoTr);

	triedy[ix_tr].menoNadTr=triedy[ixNT].menoTr;
	triedy[ix_tr].pristup=mod;
	triedy[ix_tr].pocDatClenov=0;
	triedy[ix_tr].DatCleny=(dclen*)calloc(20,sizeof(dclen));
	triedy[ix_tr].pocMetod=0;
	triedy[ix_tr].Metody=(met*)calloc(20,sizeof(met));

	ix_tr++;
}

void ulozDC(char *meno,char *sig,uint mod)
{
	int i,poc,ixt;
	ixt=ix_tr-1;
	poc=triedy[ixt].pocDatClenov;
	
	for(i=0;i<poc;i++)
		if(strcmp(triedy[ixt].DatCleny[i].meno,meno)==0)
		{
			xxerror("Viacnasobna deklaracia datoveho clena:",meno);
			return;
		}

	if(testEM(meno,ixt))
		{xxerror("Existuje (zdedena) metoda:",meno);return;}

	triedy[ixt].DatCleny[poc].meno=meno;
	triedy[ixt].DatCleny[poc].pristup=mod;

	if(mod&MOD_ABSTRACT)
		xxerror("Datovy clen nemoze byt abstraktny:",meno);

	triedy[ixt].DatCleny[poc].signatura=sig;
	triedy[ixt].pocDatClenov++;
}


void ulozMet(char *meno,char *sigP,char *sigR,uint mod)
{
	int i,ixt;
	char *sig;
	ixt=ix_tr-1;
	ix_m=triedy[ixt].pocMetod;
	
	for(i=0;i<ix_m;i++)
		if(strcmp(triedy[ixt].Metody[i].meno,meno)==0 &&
			strcmp(triedy[ixt].Metody[i].signaturaP,sigP)==0)
			{
				xxerror("Viacnasobna deklaracia metody:",meno);
				return;
			}

	if(testEDC(meno,ix_tr))
	{
		xxerror("Existuje (zdedeny) datovy clen:",meno);
		return;
	}

	triedy[ixt].Metody[ix_m].meno=meno;
	triedy[ixt].Metody[ix_m].pristup=mod;
	triedy[ixt].Metody[ix_m].signaturaP=sigP;
	triedy[ixt].Metody[ix_m].signaturaR=sigR;
	triedy[ixt].Metody[ix_m].kod=NULL;
	triedy[ixt].pocMetod++;
	navestie=0;
}

int testEDC(char *m,int ix)
{
	int i;
	for(i=0;i<triedy[ix].pocDatClenov;i++)
		if(strcmp(m,triedy[ix].DatCleny[i].meno)==0)
			return 1;

	if(triedy[ix].ixntr<0)
		return 0;
	
	return testEDC(m,triedy[ix].ixntr);
}

int testEM(char *m,int ix)
{
	int i;
	for(i=0;i<triedy[ix].pocMetod;i++)
		if(strcmp(m,triedy[ix].Metody[i].meno)==0)
			return 1;

	if(triedy[ix].ixntr<0)return 0;
return testEM(m,triedy[ix].ixntr);
}







void ulozLP(char *meno,char *sig)
{
	LP[ixlp].meno=meno;
	LP[ixlp].sig=sig;
	LP[ixlp].index=szlp;
	szlp++;
	
	if(sig[0]=='D') /* sizeof(double)=2 */
		szlp++;

	ixlp++;
}

void initLP(int modstatic)
{
	ixlp=0;
	if(modstatic)
		szlp=0;	//staticka metoda - nema implicitne THIS
	else
		szlp=1;	//virtualna metoda - ma implicitne THIS
}

void ukoncLP(void)
{
	triedy[ix_tr-1].Metody[ix_m].velkostLP=szlp+1;
}

int najdiLP(char *meno)
{
	int i;
	
	for(i=0;i<ixlp;i++)
		if(strcmp(meno,LP[i].meno)==0)
			return(i);

	return (-1);
}


ATRV binAritVyraz(ATRV v1,ATRV v2,char* int_op,char* dbl_op)
{
	ATRV lv;
	lv.kod=(char*)malloc(10+strlen(v1.kod)+strlen(v2.kod));

	if(v1.sig[0]=='I' && v2.sig[0]=='I')
	{
		lv.sig=v1.sig;
		sprintf(lv.kod,"%s%s\t%s\n",v1.kod,v2.kod,int_op);
	}
	else if(v1.sig[0]=='D' && v2.sig[0]=='D')
	{
		lv.sig=v1.sig;
		sprintf(lv.kod,"%s%s\t%s\n",v1.kod,v2.kod,dbl_op);
	}
	else
	{
		yyerror("Nekompatibilne vyrazy pre aritmeticky operator");
		lv.sig="?";
		sprintf(lv.kod,"%s%s????\n",v1.kod,v2.kod);
	}
	free(v1.kod);	free(v2.kod);

	return lv;
}


ATRV binRelVyraz(ATRV v1,ATRV v2,char* int_op,char* dbl_op,char* obj_op)
{
	ATRV lv;int n1,n2;
	lv.kod=(char*)malloc(100+strlen(v1.kod)+strlen(v2.kod));
	lv.sig="Z";
	
	if(v1.sig[0]=='I' && v2.sig[0]=='I')
	{
		n1=navestie++;n2=navestie++;
		sprintf(lv.kod,"%s%s\t%s\t.%d\n\tbipush\t0\n\tgoto\t.%d\n.%d\n\tbipush\t1\n.%d\n",
				v1.kod,v2.kod,int_op,n1,n2,n1,n2);
	}
	else if(v1.sig[0]=='D' && v2.sig[0]=='D')
	{
		n1=navestie++;n2=navestie++;
		sprintf(lv.kod,"%s%s\t%s\t.%d\n\tbipush\t0\n\tgoto\t.%d\n.%d\n\tbipush\t1\n.%d\n",
				v1.kod,v2.kod,dbl_op,n1,n2,n1,n2);
	}
	else if(v1.sig[0]=='L' && v2.sig[0]=='L')
	{
		if(obj_op)
		{
			n1=navestie++;n2=navestie++;
			sprintf(lv.kod,"%s%s\t%s\t.%d\n\tbipush\t0\n\tgoto\t.%d\n.%d\n\tbipush\t1\n.%d\n",
		 			v1.kod,v2.kod,obj_op,n1,n2,n1,n2);
		}
		else
		{
			yyerror("Nedovolene porovnanie referencii");
			sprintf(lv.kod,"%s%s????\n");
		}
	}
	else
	{
		yyerror("Nekompatibilne vyrazy pre relacny operator");
		sprintf(lv.kod,"%s%s????\n",v1.kod,v2.kod);
	}
	free(v1.kod);free(v2.kod);
	
	return lv;
}

ATRV binLogVyraz(ATRV v1,ATRV v2,char* log_op)
{
	ATRV lv;
	lv.kod=(char*)malloc(10+strlen(v1.kod)+strlen(v2.kod));

	if(v1.sig[0]=='Z' && v2.sig[0]=='Z')
	{
		lv.sig=v1.sig;
		sprintf(lv.kod,"%s%s\t%s\n",v1.kod,v2.kod,log_op);
	}
	else
	{
		yyerror("Nekompatibilne vyrazy pre logicky operator");
		lv.sig="?";sprintf(lv.kod,"%s%s????\n",v1.kod,v2.kod);
	}

	free(v1.kod);free(v2.kod);

	return lv;
}

ATRV unAritVyraz(ATRV v1,char* int_op,char* dbl_op)
{
	ATRV lv;
	
	if(v1.sig[0]=='I')
	{
		lv.sig=v1.sig;
		if(int_op)
		{
			lv.kod=(char*)malloc(10+strlen(v1.kod));
			sprintf(lv.kod,"%s\t%s\n",v1.kod,int_op);
			free(v1.kod);
		}
		else lv.kod=v1.kod;
	}
	else if(v1.sig[0]=='D')
	{
		lv.sig=v1.sig;
		if(dbl_op)
		{
			lv.kod=(char*)malloc(10+strlen(v1.kod));
			sprintf(lv.kod,"%s\t%s\n",v1.kod,dbl_op);
			free(v1.kod);
		}
		else lv.kod=v1.kod;
	}
	else
	{
		yyerror("Nekompatibilny vyraz pre unarny aritmeticky operator");
		
		lv.sig="?";
		lv.kod=v1.kod;
	}
	
	return lv;
}

char *vratMod(uint mod)
{
	static char textmod[80];
	textmod[0]='\0';
	if(mod&MOD_PUBLIC)strcat(textmod,"public ");
	if(mod&MOD_PROTECTED)strcat(textmod,"protected ");
	if(mod&MOD_PRIVATE)strcat(textmod,"private ");
	if(mod&MOD_STATIC)strcat(textmod,"static ");
	if(mod&MOD_FINAL)strcat(textmod,"final ");
	if(mod&MOD_ABSTRACT)strcat(textmod,"abstract ");
	if(mod&MOD_NATIVE)strcat(textmod,"native ");
	
	return textmod;
}


void vypisTriedy(int ix)
{
	int i,ixn;char mt[80];
	FILE *vyst;
	if(triedy[ix].pristup&MOD_NATIVE)
		return;
	strcpy(mt,triedy[ix].menoTr);strcat(mt,".jal");
	
	;
	if((vyst=fopen(mt,"w"))==NULL)
		xxerror("Nemozno vytvorit subor:",mt);

	fprintf(vyst,".source %s\n",src);
	fprintf(vyst,".class %s %s\n",vratMod(triedy[ix].pristup),triedy[ix].menoTr);

	ixn=triedy[ix].ixntr;
	fprintf(vyst,".super %s\n",triedy[ixn].javaTr);

	for(i=0;i<triedy[ix].pocDatClenov;i++)
	{

		if(strcmp(triedy[ix].DatCleny[i].signatura,"R")==0)
		{
			fprintf(vyst,".field\t%s_Lx\tI\n",			triedy[ix].DatCleny[i].meno);
			fprintf(vyst,".field\t%s_Ly\tI\n",			triedy[ix].DatCleny[i].meno);
			fprintf(vyst,".field\t%s_a\tI\n",			triedy[ix].DatCleny[i].meno);
			fprintf(vyst,".field\t%s_b\tI\n",			triedy[ix].DatCleny[i].meno);
			fprintf(vyst,".field\t%s_Col\tLjava/awt/Color;\n",triedy[ix].DatCleny[i].meno);
			fprintf(vyst,".field\t%s_Font\tLjava/awt/Font;\n",triedy[ix].DatCleny[i].meno);
		}
		else
		{
			fprintf(vyst,".field %s %s %s\n",vratMod(triedy[ix].DatCleny[i].pristup),
				triedy[ix].DatCleny[i].meno,triedy[ix].DatCleny[i].signatura);
		}
	}

	for(i=0;i<triedy[ix].pocMetod;i++)
	{
		fprintf(vyst,"\n.method %s %s %s%s\n",vratMod(triedy[ix].Metody[i].pristup),
			triedy[ix].Metody[i].meno,
			triedy[ix].Metody[i].signaturaP,triedy[ix].Metody[i].signaturaR);
		if(triedy[ix].Metody[i].kod!=NULL)
		{
			fprintf(vyst,"\t.stack 150\n");
			/*fprintf(vyst,"\t.locals 3\n");*/
			fprintf(vyst,"\t.locals %d\n",triedy[ix].Metody[i].velkostLP); 
			fprintf(vyst,triedy[ix].Metody[i].kod);
		}
		fprintf(vyst,".endm\n");
	}

	fclose(vyst);
}	
