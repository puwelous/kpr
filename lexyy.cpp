/* --------------------------------------------------------------------- 
//    Subor:  "lexyy.cpp" 
// 
//    Tento subor bol generovany programom KPI-Lex 1.00.
// 
//    Pouzity specifikacny subor: "zadanie.l".
//    Pouzity skeleton subor:      "E:\Programy\PPVP\ncform".
// 
//    Vytvorene:  Wed Dec 05 03:05:46 2012
// 
// ------------------------------------------------------------------- */ 

/* -------------------------------------------------------------------------- 
// Nasledujuca cast tohoto suboru je generovana priamo programom KPI-Lex.
-------------------------------------------------------------------------- */

/* --- Standardne hlavickove subory ----------- */
#include <stdio.h> 
#include <alloc.h> 

/* --- Makro definicie ----------------------------------- */
#define   U(x)       x
#define   NLSTATE    yyprevious = YYNEWLINE
#define   BEGIN      yybgin = yysvec + 1 +
#define   INITIAL    0
#define   YYLERR     yysvec
#define   YYSTATE    ( yyestate - yysvec - 1 )
#define   YYOPTIM    1
#define   YYLMAX     200

#define   output( c )   putc( c, yyout )
#define   input()       ( ( ( yytchar = yysptr > yysbuf ? U( *--yysptr ) : getc( yyin ) ) == 10 ? ( yylineno++, yytchar ) : yytchar ) == EOF ? 0 : yytchar ) 
#define   unput( c )    {  yytchar = ( c ); if ( yytchar == '\n') yylineno--; *yysptr++ = yytchar; }
#define   yymore()      ( yymorfg = 1 )
#define   ECHO          fprintf( yyout,"%s", yytext )
#define   REJECT        { nstr = yyreject(); goto yyfussy; }

/* -------- Prototypy lokalnych funkcii --------------- */
/* --- ( generovane automaticky programom KPI-Lex------ */

int   yylook( void ); 
int   yyback( int  *p, int  m );
int  yyinput( void );
void  yyoutput( int  c );
void  yyunput( int c );

/* ---  Definicie premennych a ine definicie ------------- */

FILE    *yyin   =   {stdin},  
        *yyout  =   {stdout} ;

struct  yysvf  { 
    struct  yywork   *yystoff;
    struct  yysvf    *yyother;
    int              *yystops;
};

struct           yysvf   *yyestate;

extern  struct   yysvf    yysvec[],
                         *yybgin;

int   yyleng;  
int   yymorfg; 
int   yytchar; 

extern  char    yytext[];
extern  char    yysbuf[];
extern  char   *yysptr;
extern  int     yylineno;


#include <string.h>

int it,ik,riadok=1;
extern YYSTYPE yylval;
int yywrap(void);
struct { char *txt;int kod;} ks[]={
	"applet",APPLET,
	"farba",FARBA,
	"int",INT,
	"boolean",BOOLEAN,
	"void",VOID,	
	
	"init",INIT,
	"move",MOVE,
	"setcolor",SETCOLOR,
	"resize",RESIZE,
	"show",SHOW,
	"line",LINE,
	"from",FROM,
	"to",TO,
	"in",IN,
	"print",PRINT,
	"at", AT,
	"if",IF,
	"else",ELSE,
	"while",WHILE,
	"do",DO,
	"for",FOR,
	"with",WITH,
	"color", COLOR,
	"output", OUTPUT,
	"end", END,
	"begin", BEGINN,

	"inti", INTI,
	"rectangle", RECTANGLE,
	"endapplet", ENDAPPLET,
	"painttt", PAINTTT,

	NULL,0
	};
struct { char *txt;char *meno;} farba[]={
			"BLACK","black",
			"BLUE","blue",
			"GREEN","green",
			"CYAN","cyan",
			"RED","red",
			"MAGENTA","magenta",
			"LIGHTGRAY","lightGray",
			"DARKGRAY","darkGray",
			"GRAY","gray",
			"ORANGE","orange",
			"PINK","pink",
			"YELLOW","yellow",
			"WHITE","white",
			NULL,NULL
	};

# define YYNEWLINE 10
yylex(){
int nstr; extern int yyprevious;

#pragma warn -rch

   while ( ( nstr = yylook() ) >= 0 )  { 
yyfussy:
        switch( nstr )  {
            case 0:
                     if ( yywrap() ) 
                          return( 0 );
                           break;
case 1:
	return(PLUSPLUS);
break;
case 2:
	return(MINUSMINUS);
break;
case 3:
	return(CIARKA);
break;
case 4:
	return(BCIARKA);
break;
case 5:
	return(LOZAT);
break;
case 6:
	return(POZAT);
break;
case 7:
	return(LZZAT);
break;
case 8:
	return(PZZAT);
break;
case 9:
	return(LHZAT);
break;
case 10:
	return(PHZAT);
break;
case 11:
	return(BODKA);
break;
case 12:
	return(ROVNE);
break;
case 13:
	return(PLUS);
break;
case 14:
	return(MINUS);
break;
case 15:
	return(KRAT);
break;
case 16:
	return(DEL);
break;
case 17:
	return(MENSI);
break;
case 18:
	return(MENSIROVNY);
break;
case 19:
	return(VACSI);
break;
case 20:
	return(VACSIROVNY);
break;
case 21:
	return(EQUIVAL);
break;
case 22:
	return(NEEQ);
break;
case 23:
	return(OR);
break;
case 24:
	return(AND);
break;
case 25:
	return(ZVYS);
break;
case 26:
	return(NOT);
break;
case 27:
	return(OTAZ);
break;
case 28:
	return(DBOD);
break;
case 29:
{ yytext[yyleng-1]=0;
		  yylval.txt=strdup(&yytext[1]);
		  return(TEXT);
		}
break;
case 30:
	{ /* test na klucove slovo */
		  for(ik=0;ks[ik].txt!=NULL;ik++)
			if(strcmp(ks[ik].txt,yytext)==0)return ks[ik].kod;

		  /* test na druh farby */
		  for(ik=0;farba[ik].txt!=NULL;ik++)
			if(strcmp(farba[ik].txt,yytext)==0)
			{
			    yylval.meno=strdup(farba[ik].meno);
			    return (FARBA);
			}

		  /* ked nic ine tak to bude identifikator*/
		  yylval.meno=strdup(yytext);
		  return(ID);
		}
break;
case 31:
	{ sscanf(yytext,"%d",&yylval.ihod);return(INT_LITERAL);}
break;
case 32:
	;
break;
case 33:
	{ riadok++;}
break;
case 34:
;
break;
case 35:
	{ xxerror("Nedovoleny znak: ",yytext);}
break;

            case -1: 
                     break; 

            default: 
                     fprintf( yyout, "bad switch yylook  %d", nstr ); 
                     break;
        } /* end of switch */
   } /* end of while */

   return ( 0 ); 
#pragma warn +rch

}/* end of yylex */

int yywrap(void){return(1);}

/* --- Prekladove tabulky -- Cast generovana programom KPI-Lex --- */

int   yyvstop[]  =  {
                         0,
                         /* Akcia pre stav 2 */
                         35,
                         0,
                         /* Akcia pre stav 3 */
                         32,
                         35,
                         0,
                         /* Akcia pre stav 4 */
                         33,
                         0,
                         /* Akcia pre stav 5 */
                         26,
                         35,
                         0,
                         /* Akcia pre stav 6 */
                         35,
                         0,
                         /* Akcia pre stav 7 */
                         25,
                         35,
                         0,
                         /* Akcia pre stav 8 */
                         35,
                         0,
                         /* Akcia pre stav 9 */
                         5,
                         35,
                         0,
                         /* Akcia pre stav 10 */
                         6,
                         35,
                         0,
                         /* Akcia pre stav 11 */
                         15,
                         35,
                         0,
                         /* Akcia pre stav 12 */
                         13,
                         35,
                         0,
                         /* Akcia pre stav 13 */
                         3,
                         35,
                         0,
                         /* Akcia pre stav 14 */
                         14,
                         35,
                         0,
                         /* Akcia pre stav 15 */
                         11,
                         35,
                         0,
                         /* Akcia pre stav 16 */
                         16,
                         35,
                         0,
                         /* Akcia pre stav 17 */
                         31,
                         35,
                         0,
                         /* Akcia pre stav 18 */
                         28,
                         35,
                         0,
                         /* Akcia pre stav 19 */
                         4,
                         35,
                         0,
                         /* Akcia pre stav 20 */
                         17,
                         35,
                         0,
                         /* Akcia pre stav 21 */
                         12,
                         35,
                         0,
                         /* Akcia pre stav 22 */
                         19,
                         35,
                         0,
                         /* Akcia pre stav 23 */
                         27,
                         35,
                         0,
                         /* Akcia pre stav 24 */
                         30,
                         35,
                         0,
                         /* Akcia pre stav 25 */
                         9,
                         35,
                         0,
                         /* Akcia pre stav 26 */
                         10,
                         35,
                         0,
                         /* Akcia pre stav 27 */
                         7,
                         35,
                         0,
                         /* Akcia pre stav 28 */
                         35,
                         0,
                         /* Akcia pre stav 29 */
                         8,
                         35,
                         0,
                         /* Akcia pre stav 30 */
                         22,
                         0,
                         /* Akcia pre stav 32 */
                         29,
                         0,
                         /* Akcia pre stav 33 */
                         24,
                         0,
                         /* Akcia pre stav 34 */
                         1,
                         0,
                         /* Akcia pre stav 35 */
                         2,
                         0,
                         /* Akcia pre stav 36 */
                         34,
                         0,
                         /* Akcia pre stav 37 */
                         31,
                         0,
                         /* Akcia pre stav 38 */
                         18,
                         0,
                         /* Akcia pre stav 39 */
                         21,
                         0,
                         /* Akcia pre stav 40 */
                         20,
                         0,
                         /* Akcia pre stav 41 */
                         30,
                         0,
                         /* Akcia pre stav 42 */
                         23,
                         0,
                         0
};  /* koniec tabulky "yyvstop" */ 

#define   YYTYPE    char
struct  yywork   { 
                   YYTYPE verify,  
                          advance; 
}  yycrank[]  =  { 
                      0,0,	0,0,	1,3,	0,0,	
                      7,32,	0,0,	0,0,	0,0,	
                      0,0,	37,37,	1,4,	1,5,	
                      7,32,	7,0,	0,0,	0,0,	
                      0,0,	37,37,	37,0,	0,0,	
                      0,0,	0,0,	0,0,	0,0,	
                      0,0,	0,0,	0,0,	0,0,	
                      0,0,	0,0,	32,0,	33,0,	
                      0,0,	0,0,	1,6,	1,7,	
                      0,0,	7,33,	1,8,	1,9,	
                      9,34,	1,10,	1,11,	1,12,	
                      1,13,	1,14,	1,15,	1,16,	
                      1,17,	1,18,	13,35,	7,32,	
                      15,36,	17,37,	32,33,	33,33,	
                      37,37,	0,0,	0,0,	1,19,	
                      1,20,	1,21,	1,22,	1,23,	
                      1,24,	6,31,	1,25,	21,39,	
                      7,32,	22,40,	2,6,	2,7,	
                      23,41,	37,37,	2,8,	2,9,	
                      0,0,	2,10,	2,11,	2,12,	
                      2,13,	2,14,	2,15,	2,16,	
                      2,17,	0,0,	0,0,	0,0,	
                      0,0,	0,0,	0,0,	0,0,	
                      1,26,	0,0,	1,27,	2,19,	
                      2,20,	2,21,	2,22,	2,23,	
                      2,24,	18,38,	18,38,	18,38,	
                      18,38,	18,38,	18,38,	18,38,	
                      18,38,	18,38,	18,38,	0,0,	
                      0,0,	0,0,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      1,28,	1,29,	1,30,	29,43,	
                      2,26,	0,0,	2,27,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	0,0,	0,0,	0,0,	
                      2,28,	2,29,	2,30,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	25,42,	25,42,	25,42,	
                      25,42,	0,0,	0,0,	0,0,	
                      0,0
};  /* koniec tabulky  "yycrank" */ 

struct  yysvf   yysvec[]  =  {
0,	0,	0,
yycrank+-1,	0,		0,			/* stav 0 */
yycrank+-37,	yysvec+1,	0,			/* stav 1 */
yycrank+0,	0,		yyvstop+1,		/* stav 2 */
yycrank+0,	0,		yyvstop+3,		/* stav 3 */
yycrank+0,	0,		yyvstop+6,		/* stav 4 */
yycrank+4,	0,		yyvstop+8,		/* stav 5 */
yycrank+-3,	0,		yyvstop+11,		/* stav 6 */
yycrank+0,	0,		yyvstop+13,		/* stav 7 */
yycrank+2,	0,		yyvstop+16,		/* stav 8 */
yycrank+0,	0,		yyvstop+18,		/* stav 9 */
yycrank+0,	0,		yyvstop+21,		/* stav 10 */
yycrank+0,	0,		yyvstop+24,		/* stav 11 */
yycrank+7,	0,		yyvstop+27,		/* stav 12 */
yycrank+0,	0,		yyvstop+30,		/* stav 13 */
yycrank+7,	0,		yyvstop+33,		/* stav 14 */
yycrank+0,	0,		yyvstop+36,		/* stav 15 */
yycrank+6,	0,		yyvstop+39,		/* stav 16 */
yycrank+53,	0,		yyvstop+42,		/* stav 17 */
yycrank+0,	0,		yyvstop+45,		/* stav 18 */
yycrank+0,	0,		yyvstop+48,		/* stav 19 */
yycrank+6,	0,		yyvstop+51,		/* stav 20 */
yycrank+8,	0,		yyvstop+54,		/* stav 21 */
yycrank+11,	0,		yyvstop+57,		/* stav 22 */
yycrank+0,	0,		yyvstop+60,		/* stav 23 */
yycrank+66,	0,		yyvstop+63,		/* stav 24 */
yycrank+0,	0,		yyvstop+66,		/* stav 25 */
yycrank+0,	0,		yyvstop+69,		/* stav 26 */
yycrank+0,	0,		yyvstop+72,		/* stav 27 */
yycrank+3,	0,		yyvstop+75,		/* stav 28 */
yycrank+0,	0,		yyvstop+77,		/* stav 29 */
yycrank+0,	0,		yyvstop+80,		/* stav 30 */
yycrank+-20,	yysvec+7,	0,			/* stav 31 */
yycrank+-21,	yysvec+7,	yyvstop+82,		/* stav 32 */
yycrank+0,	0,		yyvstop+84,		/* stav 33 */
yycrank+0,	0,		yyvstop+86,		/* stav 34 */
yycrank+0,	0,		yyvstop+88,		/* stav 35 */
yycrank+-8,	0,		yyvstop+90,		/* stav 36 */
yycrank+0,	yysvec+18,	yyvstop+92,		/* stav 37 */
yycrank+0,	0,		yyvstop+94,		/* stav 38 */
yycrank+0,	0,		yyvstop+96,		/* stav 39 */
yycrank+0,	0,		yyvstop+98,		/* stav 40 */
yycrank+0,	yysvec+25,	yyvstop+100,		/* stav 41 */
yycrank+0,	0,		yyvstop+102,		/* stav 42 */
0,	0,	0
};  /* koniec tabulky "yysvec" */ 

struct  yywork   *yytop  = yycrank + 188;
struct  yysvf    *yybgin = yysvec  + 1;

char  yymatch[]  =  {
                     00  ,01  ,01  ,01  ,01  ,01  ,01  ,01  ,
                     01  ,011 ,012 ,01  ,01  ,01  ,01  ,01  ,
                     01  ,01  ,01  ,01  ,01  ,01  ,01  ,01  ,
                     01  ,01  ,01  ,01  ,01  ,01  ,01  ,01  ,
                     011 ,01  ,01  ,01  ,01  ,01  ,01  ,01  ,
                     01  ,01  ,01  ,01  ,01  ,01  ,01  ,01  ,
                     '0' ,'0' ,'0' ,'0' ,'0' ,'0' ,'0' ,'0' ,
                     '0' ,'0' ,01  ,01  ,01  ,01  ,01  ,01  ,
                     01  ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,01  ,01  ,01  ,01  ,01  ,
                     01  ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,'A' ,
                     'A' ,'A' ,'A' ,01  ,01  ,01  ,01  ,01  ,
                     0
}; /* koniec tabulky "yymatch" */ 

char  yyextra[]  =   {
                         0, 0, 0, 0, 0, 0, 0, 0, 
                         0, 0, 0, 0, 0, 0, 0, 0, 
                         0, 0, 0, 0, 0, 0, 0, 0, 
                         0, 0, 0, 0, 0, 0, 0, 0, 
                         0, 0, 0, 0, 0, 0, 0, 0, 
                         0
};  /* koniec tabulky "yyextra" */ 


/* ----------------------------------------------------------------------- */
/*             Section copied from skeleton file  "ncform"                 */
/* ----------------------------------------------------------------------- */

#define    YYU(x)    x
#define    NLSTATE   yyprevious = YYNEWLINE

extern struct   yysvf   *yyestate;
struct          yysvf   *yylstate [YYLMAX],
		       **yylsp,
		       **yyolsp;

int      yylineno  = 1;

char     yytext[ YYLMAX ];
char     yysbuf[YYLMAX];
char    *yysptr = yysbuf;
int     *yyfnd;
int      yyprevious = YYNEWLINE;


int  yylook( void )
{
    register struct  yysvf    *yystate, **lsp;
    register struct  yywork   *yyt;

    struct           yysvf    *yyz;
    struct           yywork   *yyr;

    int      yych;

#ifdef  LEXDEBUG
    int      debug;
#endif

    char    *yylastch;


    /* start off machines */

#ifdef LEXDEBUG
    debug  =  0;
#endif

    if ( !yymorfg  )  {
	yylastch  =  yytext;
    }
    else  {
	yymorfg  =  0;
	yylastch = yytext + yyleng;
    }

    for(;;)  {
	lsp = yylstate;
	yyestate = yystate = yybgin;

	if ( yyprevious == YYNEWLINE )
	   yystate++;

	for (;;)  {

#ifdef LEXDEBUG
	    if ( debug )
	       fprintf( yyout, "state %d\n", yystate - yysvec - 1 );
#endif
	    yyt = yystate->yystoff;

	    /* may not be any transitions */
	    if ( yyt == yycrank )  {
		yyz = yystate->yyother;

		if ( yyz == 0 )
		   break;

		if ( yyz->yystoff == yycrank )
		   break;
	    }

	    *yylastch++ = yych = input();

tryagain:

#ifdef LEXDEBUG
	    if ( debug ) {
	       fprintf( yyout, "char " );
	       allprint( yych );
	       putchar( '\n' );
	    }
#endif

	    yyr = yyt;

	    if ( (int) yyt > (int) yycrank )  {
		yyt = yyr + yych;

		if ( yyt <= yytop &&  yyt->verify + yysvec == yystate)  {

		    /* error transitions */
		    if ( yyt->advance + yysvec == YYLERR )  {
		       unput( *--yylastch );
		       break;
		    }

		    *lsp++ = yystate = yyt->advance + yysvec;
		    goto contin;
		}
	    }

#ifdef YYOPTIM

	    /* r < yycrank */
	    else if ( (int) yyt < (int) yycrank )  {
		yyt = yyr = yycrank + ( yycrank - yyt );

#ifdef LEXDEBUG
		if( debug )
		   fprintf( yyout, "compressed state\n" );
#endif

		yyt = yyt + yych;

		if ( yyt <= yytop  &&  yyt->verify + yysvec == yystate )  {

		   /* error transitions */
		   if ( yyt->advance + yysvec == YYLERR )  {
		      unput( *--yylastch );
		      break;
		   }

		   *lsp++ = yystate = yyt->advance + yysvec;
		   goto contin;
		}

		yyt = yyr + YYU( yymatch[yych] );

#ifdef LEXDEBUG
		if ( debug )  {
		   fprintf( yyout, "try fall back character " );
		   allprint( YYU( yymatch[yych] ) );
		   putchar( '\n' );
		}
#endif

		if ( yyt <= yytop  &&  yyt->verify + yysvec == yystate )  {

		   /* error transition */
		   if ( yyt->advance + yysvec == YYLERR )  {
		      unput( *--yylastch );
		      break;
		   }

		   *lsp++ = yystate = yyt->advance + yysvec;
		   goto contin;
		}
	    }

	    if (
		  ( yystate = yystate->yyother) != 0  &&
		  (yyt= yystate->yystoff) != yycrank
	       )  {

#ifdef  LEXDEBUG
	       if ( debug )
		  fprintf( yyout, "fall back to state %d\n",
				  yystate - yysvec - 1 );
#endif
	       goto  tryagain;
	    }

#endif
	    else  {
		unput( *--yylastch );
		break;
	    }

contin:

#ifdef  LEXDEBUG
	    if ( debug )  {
	       fprintf( yyout, "state %d char ", yystate - yysvec - 1 );
	       allprint( yych );
	       putchar( '\n' );
	    }
#endif
	    ;     /* empty command */

	}

#ifdef LEXDEBUG
	if ( debug )  {
	   fprintf( yyout, "stopped at %d with ", *(lsp - 1) - yysvec - 1 );
	   allprint( yych );
	   putchar( '\n' );
	}
#endif

	while ( lsp-- > yylstate )  {

	    *yylastch--  =  0;

	    if (
		 *lsp  !=  0  &&
		 ( yyfnd = (*lsp)->yystops ) != 0 &&
		 *yyfnd > 0
	       )  {

	       yyolsp = lsp;

	       /* must backup */
	       if ( yyextra[*yyfnd] )   {

		  while (
			  yyback( (*lsp)->yystops, -*yyfnd ) != 1 &&
			  lsp > yylstate
			)  {

			lsp--;
			unput( *yylastch-- );
		  }
	       }

	       yyprevious = YYU( *yylastch );
	       yylsp  = lsp;
	       yyleng = yylastch - yytext + 1;
	       yytext[yyleng]  =  0;

#ifdef LEXDEBUG
	       if ( debug )  {
		  fprintf( yyout, "\nmatch " );
		  sprint( yytext );
		  fprintf( yyout, " action %d\n", *yyfnd );
		}
#endif

		return( *yyfnd++ );
	    }

	    unput( *yylastch );
	}

	if ( yytext[0] == 0  )  {    /* && feof(yyin) */

	   yysptr = yysbuf;

	   return ( 0 );
	}

	yyprevious = yytext[0] = input();

	if ( yyprevious > 0 )
	   output( yyprevious );

	yylastch = yytext;

#ifdef LEXDEBUG
	if ( debug )
	   putchar( '\n' );
#endif

    }

}



int  yyback( int  *p, int  m )
{
    if ( p  == 0 )
       return ( 0 );

    while ( *p ) {
       if ( *p++  ==  m )
	  return ( 1 );
    }

    return ( 0 );
}


/* the following are only used in the lex library */

int  yyinput( void )
{
    return ( input() );
}


void  yyoutput( int  c )
{
    output( c );
}


void  yyunput( int c )
{
    unput( c );
}


